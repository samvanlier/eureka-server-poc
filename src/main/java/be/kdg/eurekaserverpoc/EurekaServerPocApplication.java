package be.kdg.eurekaserverpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServerPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerPocApplication.class, args);
	}
}
